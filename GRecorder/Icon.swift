import UIKit

class Icon {

    // Generic icons
    static let plus: UIImage = UIImage(named: "plus")!

    // Media icons
    static let play: UIImage = UIImage(named: "play")!
    static let stop: UIImage = UIImage(named: "stop")!
    static let prev: UIImage = UIImage(named: "prev")!
    static let next: UIImage = UIImage(named: "next")!
    static let pause: UIImage = UIImage(named: "pause")!
    static let record: UIImage = UIImage(named: "record")!

}
