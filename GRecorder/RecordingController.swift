import UIKit
import SnapKit
import RxSwift
import AVFoundation

protocol RecordingControllerDelegate {

    func recordingDeleted()

}

class RecordingController: UIViewController {

    var progressBar: ProgressBar!
    var playButton: UIButton!
    var prevButton: UIButton!
    var nextButton: UIButton!
    var durationPassedLabel: UILabel!
    var durationTotalLabel: UILabel!

    let container: Container
    let delegate: RecordingControllerDelegate

    var recordNode: RecordNode
    var player: Player

    let disposeBag = DisposeBag()

    var record: Record {
        return recordNode.record
    }

    init(container: Container, recordNode: RecordNode, delegate: RecordingControllerDelegate) {
        self.container = container
        self.recordNode = recordNode
        self.delegate = delegate
        self.player = Player(url: recordNode.record.url!)

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        let formatter = Formatter()

        view.backgroundColor = .white

        // Set up navigation bar
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(onDelete))

        // Set up play button

        let playButtonSize: CGFloat = 64.0
        playButton = CircleButton(color: container.theme.mainColor, size: playButtonSize, hasShadow: false)
        playButton.setImage(Icon.play.withRenderingMode(.alwaysTemplate), for: .normal)
        playButton.layer.zPosition = 1
        view.addSubview(playButton)

        playButton.snp.makeConstraints({ make in
            make.width.height.equalTo(playButtonSize)
            make.centerX.equalTo(self.view.safeAreaLayoutGuide)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-20)
        })

        // Set up prev button

        prevButton = UIButton(type: .system)
        prevButton.setImage(Icon.prev, for: .normal)
        view.addSubview(prevButton)

        prevButton.snp.makeConstraints({ make in
            make.centerY.equalTo(playButton)
            make.right.equalTo(playButton.snp.left).offset(-20)
        })

        // Set up next button
        nextButton = UIButton(type: .system)
        nextButton.setImage(Icon.next, for: .normal)
        view.addSubview(nextButton)

        nextButton.snp.makeConstraints({ make in
            make.centerY.equalTo(playButton)
            make.left.equalTo(playButton.snp.right).offset(20)
        })

        // Set up progress bar

        progressBar = ProgressBar()
        view.addSubview(progressBar)

        progressBar.snp.makeConstraints({ make in
            make.centerY.equalTo(playButton.snp.top).offset(-20)
            make.left.right.equalTo(self.view)
        })

        // Set up duration passed label

        durationPassedLabel = UILabel()
        durationPassedLabel.textColor = .gray
        durationPassedLabel.font = .systemFont(ofSize: 15)
        view.addSubview(durationPassedLabel)

        durationPassedLabel.snp.makeConstraints({ make in
            make.top.equalTo(progressBar.snp.centerY).offset(16)
            make.left.equalTo(self.view.safeAreaLayoutGuide).offset(16)
        })

        // Set up duration total label

        durationTotalLabel = UILabel()
        durationTotalLabel.textColor = .gray
        durationTotalLabel.font = .systemFont(ofSize: 15)
        durationTotalLabel.text = formatter.time(seconds: Int(record.duration), showHours: false)
        view.addSubview(durationTotalLabel)

        durationTotalLabel.snp.makeConstraints({ make in
            make.top.equalTo(progressBar.snp.centerY).offset(16)
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-16)
        })

        // Observe play button tap
        playButton.rx.tap
            .bind(onNext: onPlayTap)
            .disposed(by: disposeBag)

        // Observe prev button tap
        prevButton.rx.tap
            .bind(onNext: onPrevTap)
            .disposed(by: disposeBag)

        // Observe next button tap
        nextButton.rx.tap
            .bind(onNext: onNextTap)
            .disposed(by: disposeBag)

        setUpPlayer(node: recordNode)
    }

    override func viewWillDisappear(_ animated: Bool) {
        player.stop()
    }

    // MARK: - Actions

    @objc func onDelete() {
        let title = "Delete this recording?"
        let message = "This action is permanent"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: onDeleteConfirmed))

        present(alert, animated: true)
    }

    func onDeleteConfirmed(action: UIAlertAction) {
        // Delete recording
        try? FileManager.default.removeItem(at: record.url!)
        container.context.delete(recordNode.record)
        container.app.saveContext()

        // Notify parent and pop view controller
        delegate.recordingDeleted()
        navigationController?.popViewController(animated: true)
    }

    func onPlayTap() {
        switch player.state {
        case .idle, .paused:
            player.play()
        case .playing:
            player.pause()
        }
    }

    func onNextState(state: PlayerState) {
        switch state {
        case .idle, .paused:
            playButton.setImage(Icon.play.withRenderingMode(.alwaysTemplate), for: .normal)
        case .playing:
            playButton.setImage(Icon.pause.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }

    func onNextProgress(progress: Float) {
        self.progressBar.value = progress

        let formatter = Formatter()
        if progress == 0.0 {
            durationPassedLabel.text = formatter.time(seconds: 0, showHours: false)
        } else {
            let secondsPassed = Int(progress * Float(record.duration))
            durationPassedLabel.text = formatter.time(seconds: secondsPassed, showHours: false)
        }
    }

    func onPrevTap() {
        guard let node = recordNode.prev else {
            return
        }

        setUpPlayer(node: node)
        player.play()
    }

    func onNextTap() {
        guard let node = recordNode.next else {
            return
        }

        setUpPlayer(node: node)
        player.play()
    }

    // MARK: - Internal methods

    func setUpPlayer(node: RecordNode) {
        recordNode = node

        player.stop()
        player = Player(url: recordNode.record.url!)

        player.stateObserver
            .bind(onNext: onNextState)
            .disposed(by: disposeBag)

        player.progressObserver
            .bind(onNext: onNextProgress)
            .disposed(by: disposeBag)

        navigationItem.title = record.name
        durationTotalLabel.text = Formatter().time(seconds: Int(record.duration), showHours: false)
    }

}
