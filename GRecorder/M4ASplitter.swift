import Foundation
import AVFoundation

struct AudioPart {

    let number: Int
    let url: URL
    let duration: Double

}

class M4ASplitter {

    func split(asset: AVAsset, count: Int) -> [AudioPart] {
        let dispatchGroup = DispatchGroup()
        var parts: [AudioPart] = []
        let partDuration = asset.duration.seconds / Double(count)

        for i in 0..<count {
            guard let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else {
                return []
            }

            let url = FileManager.default.temporaryDirectory.appendingPathComponent("\(UUID().uuidString).m4a")
            let part = AudioPart(number: i, url: url, duration: partDuration)
            parts.append(part)

            let startTime = CMTime(seconds: Double(i) * partDuration, preferredTimescale: 1)
            let endTime = CMTime(seconds: Double(i + 1) * partDuration, preferredTimescale: 1)
            let exportTimeRange = CMTimeRange(start: startTime, end: endTime)

            exporter.outputFileType = .m4a
            exporter.outputURL = url
            exporter.timeRange = exportTimeRange

            dispatchGroup.enter()

            exporter.exportAsynchronously {
                dispatchGroup.leave()
            }
        }

        dispatchGroup.wait()

        return parts
    }

}
