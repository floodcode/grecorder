import Foundation
import UIKit

struct Theme {

    let mainColor: UIColor
    let recordColor: UIColor

}
