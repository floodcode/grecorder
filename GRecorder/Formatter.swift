import Foundation

class Formatter {

    func recordDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MMMM dd HH:mm"

        return formatter.string(from: date)
    }

    func timeDecimal(value: Int) -> String {
        if value < 10 {
            return "0" + String(describing: value)
        }

        return String(describing: value)
    }

    func time(seconds: Int, showHours: Bool = true) -> String {
        var remainingSeconds = seconds

        let secondsInHour = 60 * 60
        let secondsInMinute = 60

        let hours = remainingSeconds / secondsInHour
        remainingSeconds = remainingSeconds - (hours * secondsInHour)

        let minutes = remainingSeconds / secondsInMinute
        remainingSeconds = remainingSeconds - (minutes * secondsInMinute)

        let seconds = remainingSeconds

        let hoursFormatted = timeDecimal(value: hours)
        let minutesFormatted = timeDecimal(value: minutes)
        let secondsFormatted =  timeDecimal(value: seconds)

        if !showHours {
            return "\(minutesFormatted):\(secondsFormatted)"
        }

        return "\(hoursFormatted):\(minutesFormatted):\(secondsFormatted)"
    }

}
