import UIKit
import SnapKit
import RxSwift
import AVFoundation

class RecordingsController: UITableViewController {

    let container: Container

    let disposeBag = DisposeBag()

    var records: [Record] = []

    init(container: Container) {
        self.container = container

        super.init(style: .plain)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        tableView.rowHeight = UITableView.automaticDimension

        // Set up navigation bar
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Recordings"

        // Set up new record button

        let newRecordButtonSize: CGFloat = 64.0
        let newRecordButton = CircleButton(color: container.theme.mainColor, size: newRecordButtonSize)
        newRecordButton.setImage(Icon.plus.withRenderingMode(.alwaysTemplate), for: .normal)
        newRecordButton.layer.zPosition = 1
        view.addSubview(newRecordButton)

        newRecordButton.snp.makeConstraints({ make in
            make.width.height.equalTo(newRecordButtonSize)
            make.bottom.right.equalTo(self.view.safeAreaLayoutGuide).offset(-40)
        })

        // Observe new record button tap
        newRecordButton.rx.tap
            .bind(onNext: onNewRecordTap)
            .disposed(by: disposeBag)

        // Load initial records
        updateRecords()
    }

    override func viewWillAppear(_ animated: Bool) {
        try? container.audioSession.setCategory(.ambient)
    }

    // MARK: - Table view overrides

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let record = records[indexPath.row]
        let cellItem = RecordCellItem(theme: container.theme, record: record, player: Player(url: record.url!))
        let cell = RecordCell()
        cell.setItem(item: cellItem)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let initialNode = RecordNode.fromRecords(records: records, initialNodeIndex: indexPath.row)
        let controller = RecordingController(container: container, recordNode: initialNode, delegate: self)
        navigationController?.pushViewController(controller, animated: true)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let record = records[indexPath.row]
            try? FileManager.default.removeItem(at: record.url!)
            container.context.delete(record)
            container.app.saveContext()

            records.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    // MARK: - Actions

    func onNewRecordTap() {
        let controller = RecorderController(container: container, delegate: self)
        navigationController?.pushViewController(controller, animated: true)
    }

    // MARK: - Private methods

    func updateRecords() {
        records = (try? container.context.fetch(Record.fetchRequest())) ?? []
        tableView.reloadData()
    }

}

// MARK: - RecordingControllerDelegate

extension RecordingsController: RecordingControllerDelegate {

    func recordingDeleted() {
        updateRecords()
    }

}

// MARK: - RecorderControllerDelegate

extension RecordingsController: RecorderControllerDelegate {

    func recordSaved() {
        updateRecords()
    }

    func recordFailed() {
        let alert = UIAlertController(title: "Unable to record audio", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true)
    }

}
