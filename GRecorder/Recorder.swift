import Foundation
import AVFoundation
import RxSwift
import RxRelay

enum RecorderState {

    case idle
    case recording

}

class Recorder: NSObject {

    private let disposeBag = DisposeBag()
    private let stateSubject = BehaviorSubject<RecorderState>(value: .idle)
    private let timePassedRelay = BehaviorRelay<Int>(value: 0)
    private let newRecordSubject = PublishSubject<URL>()

    private var avRecorder: AVAudioRecorder?
    private var timer: Timer?
    private var uuid = UUID()

    var state: RecorderState {
        return try! stateSubject.value()
    }

    var stateObserver: Observable<RecorderState> {
        return stateSubject.asObserver()
    }

    var timePassedObserver: Observable<Int> {
        return timePassedRelay.asObservable()
    }

    var newRecordObserver: Observable<URL> {
        return newRecordSubject.asObserver()
    }

    // MARK: - Public methods

    public func start() {
        stateSubject.onNext(.recording)

        uuid = UUID()
        let url = getTempUrl(uuid: uuid)

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        avRecorder = try! AVAudioRecorder(url: url, settings: settings)
        avRecorder?.isMeteringEnabled = true
        avRecorder?.delegate = self
        avRecorder?.record()

        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
    }

    public func stop() {
        stateSubject.onNext(.idle)
        timePassedRelay.accept(0)

        avRecorder?.stop()
        timer?.invalidate()
        timer = nil

        newRecordSubject.onNext(getTempUrl(uuid: uuid))
    }

    // MARK: - Private methods

    private func getTempUrl(uuid: UUID) -> URL {
        return FileManager.default.temporaryDirectory.appendingPathComponent("\(uuid.uuidString).m4a")
    }

    // MARK: - Actions

    @objc private func onTick() {
        let timePassedValue = timePassedRelay.value + 1
        timePassedRelay.accept(timePassedValue)

        if timePassedValue == 30 {
            stop()
        }
    }

}

extension Recorder: AVAudioRecorderDelegate {

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            stop()
        }
    }

}
