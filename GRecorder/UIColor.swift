import UIKit

extension UIColor {

    static var systemBlue: UIColor {
        return UIColor(red: 0 / 255, green: 122 / 255, blue: 255 / 255, alpha: 255 / 255)
    }

    static var systemRed: UIColor {
        return UIColor(red: 255 / 255, green: 59 / 255, blue: 48 / 255, alpha: 255 / 255)
    }

}
