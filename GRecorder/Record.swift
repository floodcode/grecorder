import UIKit

// Simple doubly linked list implementation to use as playlist of recordings

class RecordNode {

    let record: Record

    var prev: RecordNode? = nil
    var next: RecordNode? = nil

    init(record: Record) {
        self.record = record
    }

    static func fromRecords(records: [Record], initialNodeIndex: Int) -> RecordNode {
        let nodes = records.map {
            return RecordNode(record: $0)
        }

        for (i, node) in nodes.enumerated() {
            if i > 0 {
                node.prev = nodes[i - 1]
            }

            if i + 1 < nodes.count {
                node.next = nodes[i + 1]
            }
        }

        return nodes[initialNodeIndex]
    }

}

extension Record {

    var name: String {
        return "\(Formatter().recordDate(date: created ?? Date())) part \(part)"
    }

    var url: URL? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        if let id = id {
            return paths.appendingPathComponent("\(id).m4a")
        }

        return nil
    }

}
