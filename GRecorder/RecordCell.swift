import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa

class RecordCellItem {

    let theme: Theme
    let record: Record
    let player: Player

    init(theme: Theme, record: Record, player: Player) {
        self.theme = theme
        self.record = record
        self.player = player
    }

}

class RecordCell: UITableViewCell {

    var playButton: UIButton!
    var progressBar: ProgressBar!
    var item: RecordCellItem!

    let disposeBag = DisposeBag()

    init() {
        super.init(style: .default, reuseIdentifier: nil)

        accessoryType = .disclosureIndicator
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setItem(item: RecordCellItem) {
        self.item = item

        // Set up play button

        let playButtonSize: CGFloat = 48.0
        playButton = CircleButton(color: item.theme.mainColor, size: playButtonSize, hasShadow: false)
        playButton.setImage(Icon.play.withRenderingMode(.alwaysTemplate), for: .normal)
        playButton.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        playButton.imageView?.contentMode = .scaleAspectFit
        addSubview(playButton)

        playButton.snp.makeConstraints({ make in
            make.width.height.equalTo(playButtonSize)
            make.left.equalTo(self).offset(20)
            make.top.equalTo(self).offset(8)
            make.bottom.equalTo(self).offset(-8).priority(.low)
        })

        // Set up title label

        let titleLabel = UILabel()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.text = item.record.name
        addSubview(titleLabel)

        titleLabel.snp.makeConstraints({ make in
            make.centerY.equalTo(self)
            make.left.equalTo(playButton.snp.right).offset(8)
        })

        // Set up duration label

        let durationLabel = UILabel()
        durationLabel.textColor = .gray
        durationLabel.text = Formatter().time(seconds: Int(item.record.duration), showHours: false)
        addSubview(durationLabel)

        durationLabel.snp.makeConstraints({ make in
            make.centerY.equalTo(self)
            make.left.equalTo(titleLabel.snp.right).offset(8)
            make.right.equalTo(self).offset(-44)
        })

        // Set up progress bar

        progressBar = ProgressBar()
        progressBar.maximumTrackTintColor = .groupTableViewBackground
        addSubview(progressBar)

        progressBar.snp.makeConstraints({ make in
            make.left.equalTo(playButton.snp.right).offset(8)
            make.right.equalTo(self).offset(-44)
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
        })

        // Observe play button tap
        playButton.rx.tap
            .bind(onNext: onPlayTap)
            .disposed(by: disposeBag)

        // Observe player state
        item.player.stateObserver
            .bind(onNext: onNextState)
            .disposed(by: disposeBag)

        // Observe player progress
        item.player.progressObserver
            .bind(onNext: onNextProgress)
            .disposed(by: disposeBag)
    }

    // MARK: - Actions

    func onPlayTap() {
        switch item.player.state {
        case .idle, .paused:
            item.player.play()
        case .playing:
            item.player.pause()
        }
    }

    func onNextState(state: PlayerState) {
        switch state {
        case .idle, .paused:
            playButton.setImage(Icon.play.withRenderingMode(.alwaysTemplate), for: .normal)
        case .playing:
            playButton.setImage(Icon.pause.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }

    func onNextProgress(progress: Float) {
        progressBar.value = progress
    }

}
