import Foundation
import UIKit

class ProgressBar: UISlider {

    init() {
        super.init(frame: .zero)

        isUserInteractionEnabled = false
        setThumbImage(UIImage(), for: .normal)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
