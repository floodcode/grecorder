import UIKit
import SnapKit
import RxSwift
import RxRelay
import AVFoundation

enum RecorderMode {

    case idle
    case recording

}

protocol RecorderControllerDelegate {

    func recordSaved()
    func recordFailed()

}

class RecorderController: UIViewController {

    var timerLabel: UILabel!
    var recordButton: UIButton!

    let container: Container
    let delegate: RecorderControllerDelegate
    let recorder = Recorder()

    let disposeBag = DisposeBag()

    init(container: Container, delegate: RecorderControllerDelegate) {
        self.container = container
        self.delegate = delegate

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        try? container.audioSession.setCategory(.playAndRecord)

        // Request audio record permission
        container.audioSession.requestRecordPermission { [unowned self] allowed in
            DispatchQueue.main.async {
                if !allowed {
                    self.delegate.recordFailed()
                }
            }
        }

        view.backgroundColor = .white

        // Set up navigation bar
        navigationItem.title = "Record"

        // Set up time label

        timerLabel = UILabel()
        timerLabel.font = .systemFont(ofSize: 40)
        timerLabel.textColor = .darkGray
        view.addSubview(timerLabel)

        timerLabel.snp.makeConstraints({ make in
            make.top.equalTo(self.view.safeAreaLayoutGuide).offset(40)
            make.centerX.equalTo(self.view)
        })

        // Set up record button

        let recordButtonSize: CGFloat = 64.0
        recordButton = CircleButton(color: container.theme.recordColor, size: recordButtonSize)
        recordButton.setImage(Icon.record.withRenderingMode(.alwaysTemplate), for: .normal)

        view.addSubview(recordButton)

        recordButton.snp.makeConstraints({ make in
            make.width.height.equalTo(recordButtonSize)
            make.centerX.equalTo(self.view)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-40)
        })

        // Observe record button tap
        recordButton.rx.tap
            .bind(onNext: onRecordTap)
            .disposed(by: disposeBag)

        // Observe time passed
        recorder.timePassedObserver
            .bind(onNext: onNextTimePassed)
            .disposed(by: disposeBag)

        // Observe state
        recorder.stateObserver
            .bind(onNext: onNextState)
            .disposed(by: disposeBag)

        // Observe new record
        recorder.newRecordObserver
            .bind(onNext: onNextNewRecord)
            .disposed(by: disposeBag)
    }

    // MARK: - Actions

    func onRecordTap() {
        switch recorder.state {
        case .idle:
            recorder.start()
        case .recording:
            recorder.stop()
        }
    }

    func onNextState(state: RecorderState) {
        switch state {
        case .idle:
            recordButton.setImage(Icon.record.withRenderingMode(.alwaysTemplate), for: .normal)
        case .recording:
            recordButton.setImage(Icon.stop.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }

    func onNextTimePassed(value: Int) {
        timerLabel.text = Formatter().time(seconds: value)
    }

    func onNextNewRecord(url: URL) {
        let asset = AVAsset(url: url)
        let splitter = M4ASplitter()
        let parts = splitter.split(asset: asset, count: 3)

        for (i, part) in parts.enumerated() {
            let record = Record(context: container.context)
            record.id = UUID().uuidString
            record.created = Date()
            record.part = Int32(i) + 1
            record.duration = Int32(part.duration)

            print("Save #\(i)")

            try! FileManager.default.moveItem(at: part.url, to: record.url!)
        }

        try? FileManager.default.removeItem(at: url)


        container.app.saveContext()

        delegate.recordSaved()
        navigationController?.popViewController(animated: true)
    }
    
}
