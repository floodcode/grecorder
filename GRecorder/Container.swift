import Foundation
import CoreData
import AVFoundation

final class Container {

    let app: AppDelegate
    let theme: Theme
    let audioSession: AVAudioSession

    var context: NSManagedObjectContext {
        return app.persistentContainer.viewContext
    }

    init(app: AppDelegate, theme: Theme) {
        self.app = app
        self.theme = theme

        // Set up audio session
        audioSession = AVAudioSession.sharedInstance()
        try? audioSession.overrideOutputAudioPort(.speaker)
        try? audioSession.setCategory(.playAndRecord, mode: .default)
        try? audioSession.setActive(true)
    }

}
