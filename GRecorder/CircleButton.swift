import Foundation
import UIKit

class CircleButton: UIButton {

    init(color: UIColor, size: CGFloat, hasShadow: Bool = true) {
        super.init(frame: .zero)

        tintColor = .white
        backgroundColor = color
        layer.cornerRadius = size / 2

        if hasShadow {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 1 / 4
            layer.shadowOffset = .zero
            layer.shadowRadius = 4
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
