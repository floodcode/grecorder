import Foundation
import AVFoundation
import RxSwift
import RxRelay

enum PlayerState {

    case idle
    case playing
    case paused

}

class Player {

    let url: URL

    var avPlayer: AVPlayer

    private let disposeBag = DisposeBag()
    private let stateSubject = BehaviorSubject<PlayerState>(value: PlayerState.idle)
    private let progressSubject = BehaviorSubject<Float>(value: 0.0)

    var state: PlayerState {
        return try! stateSubject.value()
    }

    var stateObserver: Observable<PlayerState> {
        return stateSubject.asObserver()
    }

    var progressObserver: Observable<Float> {
        return progressSubject.asObserver()
    }

    init(url: URL) {
        self.url = url
        self.avPlayer = AVPlayer()
    }

    // MARK: - Actions

    @objc func playerDidFinishPlaying() {
        stop()
    }

    // MARK: - Public methods

    func play() {
        stateSubject.onNext(.playing)

        if avPlayer.status == .unknown {
            avPlayer = AVPlayer(url: url)
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: avPlayer.currentItem)
            addPeriodicTimeObserver()
        }

        avPlayer.play()
    }

    func pause() {
        stateSubject.onNext(.paused)

        avPlayer.pause()
    }

    func stop() {
        stateSubject.onNext(.idle)
        progressSubject.onNext(0.0)

        avPlayer = AVPlayer()
    }

    // MARK: - Private methods

    private func addPeriodicTimeObserver() {
        let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))

        avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [weak self] time in
            let currentSeconds = CMTimeGetSeconds(time)
            guard let duration = self?.avPlayer.currentItem?.duration else {
                return
            }

            let totalSeconds = CMTimeGetSeconds(duration)
            let progress = Float(currentSeconds / totalSeconds)

            self?.progressSubject.onNext(progress)
        }
    }

}
